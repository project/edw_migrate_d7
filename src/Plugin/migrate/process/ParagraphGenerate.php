<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Generate a paragraph from an array of values and return its target_id and target_revision_id.
 *
 * Available configuration keys:
 * - bundle: the paragraph bundle
 *
 * Usage:
 *
 * @code
 * process:
 *   -
 *     plugin: sub_process
 *     source: field_contact_roles
 *     process:
 *       field_treaty: field_contact_treaty
 *       field_role: field_role
 *   -
 *     plugin: paragraph_generate
 *     bundle: contact_role
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "paragraph_generate"
 * )
 */
class ParagraphGenerate extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $values = [
      'type' => $this->configuration['bundle'],
    ] + $value;

    $paragraph = Paragraph::create($values);
    $paragraph->save();
    return ['target_id' => $paragraph->id(), 'target_revision_id' => $paragraph->getRevisionId()];
  }

}
