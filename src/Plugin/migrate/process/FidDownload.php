<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\process;

use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\migrate\process\Download;
use Drupal\migrate\Row;

/**
 * Downloads a file from a HTTP(S) remote location into the local file system
 * and return the fid.
 *
 * Available configuration keys:
 * - destination: the destination dir
 * - replace_public: (optional) Replace 'public:/' in the source URL with this value
 * - guzzle_options: (optional) an array of Guzzle options
 *
 * Usage:
 *
 * @code
 * process:
 *   field_image:
 *     plugin: fid_download
 *     source: url
 *     destination: 'public://flags'
 *     replace_public: 'https://domain.org'
 *     guzzle_options:
 *       auth:
 *         username: xxx
 *         password: yyyy
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "fid_download"
 * )
 */
class FidDownload extends Download {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($value)) {
      return NULL;
    }

    // If we're stubbing a file entity, return a uri of NULL so it will get
    // stubbed by the general process.
    if ($row->isStub()) {
      return NULL;
    }
    $source = $value;
    if (!empty($this->configuration['replace_public'])) {
      $source = str_replace('public:/', $this->configuration['replace_public'], $source);
    }
    $pathinfo = pathinfo($source);
    $filename = $pathinfo['basename'];

    $filename = str_replace('download.aspx?d=', '', $filename);

    if (strlen($filename) > 30) {
      $filename = substr($filename, 0, 30);
    }
    $filename .= '.' . $pathinfo['extension'];
    $destination = $this->configuration['destination'] . '/' . $filename;

    // Modify the destination filename if necessary.
    $final_destination = $this->fileSystem->getDestinationFilename($destination, $this->configuration['file_exists']);

    // Reuse if file exists.
    if (!$final_destination) {
      return $destination;
    }

    // Try opening the file first, to avoid calling prepareDirectory()
    // unnecessarily. We're suppressing fopen() errors because we want to try
    // to prepare the directory before we give up and fail.
    $destination_stream = @fopen($final_destination, 'w');
    if (!$destination_stream) {
      // If fopen didn't work, make sure there's a writable directory in place.
      $dir = $this->fileSystem->dirname($final_destination);
      if (!$this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
        throw new MigrateException("Could not create or write to directory '$dir'");
      }
      // Let's try that fopen again.
      $destination_stream = @fopen($final_destination, 'w');
      if (!$destination_stream) {
        throw new MigrateException("Could not write to file '$final_destination'");
      }
    }

    // Stream the request body directly to the final destination stream.
    $this->configuration['guzzle_options']['sink'] = $destination_stream;

    try {
      // Make the request. Guzzle throws an exception for anything but 200.
      $this->httpClient->get($source, $this->configuration['guzzle_options']);
    }
    catch (\Exception $e) {
//      throw new MigrateException("{$e->getMessage()} ($source)");
    }

    $file = File::create([
      'uid' => 1,
      'filename' => basename($destination),
      'uri' => $destination,
      'status' => \Drupal\file\FileInterface::STATUS_PERMANENT,
    ]);
    $file->save();
    return $file->id();
  }

}
