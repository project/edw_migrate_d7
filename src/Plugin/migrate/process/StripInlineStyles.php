<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Strip CSS style from a markup
 *
 * Available configuration keys:
 * - styles: (optional) an array of CSS styles to strip. If empty, all styles will be stripped.
 *
 * Usage:
 *
 * @code
 * process:
 *   field_text:
 *     plugin: strip_inline_styles
 *     source: text
 *     styles:
 *       - color
 *       - background-color
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "strip_inline_styles"
 * )
 */
class StripInlineStyles extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($this->configuration['styles'])) {
      $value = preg_replace('/(<[^>]*) style=("[^"]+"|\'[^\']+\')([^>]*>)/i', '$1$3', $value);
      return $value;
    }
    $styles = $this->configuration['styles'];
    if (!is_array($styles)) {
      $styles = [$styles];
    }
    foreach ($styles as $removedStyle) {
      $value = preg_replace('/' . $removedStyle . '.+?;/', "", $value);
    }
    foreach ($styles as $removedStyle) {
      $value = preg_replace('/' . $removedStyle . '.+?"/', "\"", $value);
    }
    return $value;
  }

}
