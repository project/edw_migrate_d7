<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Retrieves the first deepest value of a multi-level array.
 *
 * This plugin is useful in cases where the number of array levels of the
 * data is unknown, or varies from row to row.
 *
 * Usage:
 *
 * @code
 * process:
 *   field_id:
 *     plugin: deepest_value
 *     source: field_name
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "deepest_value"
 * )
 */
class DeepestValue extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    while (is_array($value)) {
      $value = reset($value);
    }
    return $value;
  }

}
