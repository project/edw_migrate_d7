<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\taxonomy\Plugin\migrate\source\d7\TermEntityTranslation;

/**
 * Taxonomy term source from database.
 *
 * @MigrateSource(
 *   id = "edw_d7_taxonomy_term_et",
 *   source_module = "taxonomy"
 * )
 */
class EdwTermEntityTranslation extends TermEntityTranslation {

  use EdwSource;

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $ret = parent::prepareRow($row);

    $this->setUrlAlias($row);

    return $ret;
  }

}
