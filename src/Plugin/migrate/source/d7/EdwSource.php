<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\source\d7;

use Drupal\migrate\Row;

/**
 * Helpful function for D7 migrations.
 */
trait EdwSource {

  /**
   * Retrieve the URL alias.
   */
  protected function getUrlAlias(Row $row, $langcode) {
    $prefix = "node/";
    $id = $row->getSourceProperty('nid');
    if (empty($id)) {
      $id = $row->getSourceProperty('entity_id');
    }
    if (empty($id)) {
      $id = $row->getSourceProperty('tid');
    }
    if ($row->getSourceProperty('entity_type') == 'taxonomy_term' || !empty($row->getSourceProperty('tid'))) {
      $prefix = "taxonomy/term/";
    }

    $query = $this->select('url_alias', 'ua')
      ->fields('ua', ['alias']);
    $query->condition('ua.source', $prefix . $id);
    $query->condition('ua.language', $langcode);
    $query->orderBy('ua.pid', 'DESC');
    $alias = $query->execute()->fetchField();

    if (!empty($alias)) {
      return "/$alias";
    }

    if ($langcode == 'und') {
      return NULL;
    }

    if ($langcode == 'en') {
      return $this->getUrlAlias($row, 'und');
    }

    return $this->getUrlAlias($row, 'en');
  }

  /**
   * Set the URL alias.
   */
  protected function setUrlAlias(Row $row) {
    $row->setSourceProperty('alias', ['alias' => $this->getUrlAlias($row, $row->getSourceProperty('language')), 'pathauto' => 0]);
  }

  /**
   * Check if the row should be skipped.
   */
  protected function isSkippableRow($row) {
    return FALSE;
  }

}
