<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\taxonomy\Plugin\migrate\source\d7\Term;

/**
 * Drupal 7 term translation source from database.
 *
 * @MigrateSource(
 *   id = "edw_d7_taxonomy_term_i18n_translation",
 *   source_module = "node"
 * )
 */
class EdwI18nTermTranslation extends Term {

  use EdwSource;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    $query->join('i18n_string', 'i', "(i.objectid = td.tid AND i.textgroup = 'taxonomy')");
    $query->join('locales_target', 'l', 'i.lid = l.lid');
    $query->addField('l', 'language', 'translation_language');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $ret = parent::prepareRow($row);

    $row->setSourceProperty('name', $this->getTranslatedProperty($row, 'name'));
    $row->setSourceProperty('description', $this->getTranslatedProperty($row, 'description'));

    $this->setUrlAlias($row);

    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTranslatedProperty(Row $row, $property) {
    $query = $this->select('locales_target', 'l');
    $query->join('i18n_string', 'i', 'l.lid = i.lid');
    $query->fields('l', ['translation']);
    $query->condition('i.objectid', $row->getSourceProperty('tid'));
    $query->condition('i.property', $property);
    $query->condition('i.type', 'term');
    $query->condition('l.language', $row->getSourceProperty('translation_language'));
    $string = $query->execute()->fetchField();
    return $string;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['tid']['type'] = 'integer';
    $ids['language'] = [
      'alias' => 'l',
      'type' => 'string',
    ];

    return $ids;
  }

}
