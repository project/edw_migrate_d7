<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\user\Plugin\migrate\source\d7\User;

/**
 * Drupal 7 user source from database.
 *
 * @MigrateSource(
 *   id = "edw_d7_user",
 *   source_module = "user"
 * )
 */
class EdwUser extends User {

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'uid' => $this->t('User ID'),
      'name' => $this->t('Username'),
      'pass' => $this->t('Password'),
      'mail' => $this->t('Email address'),
      'signature' => $this->t('Signature'),
      'signature_format' => $this->t('Signature format'),
      'created' => $this->t('Registered timestamp'),
      'access' => $this->t('Last access timestamp'),
      'login' => $this->t('Last login timestamp'),
      'status' => $this->t('Status'),
      'timezone' => $this->t('Timezone'),
      'language' => $this->t('Language'),
      'picture' => $this->t('Picture'),
      'init' => $this->t('Init'),
      'data' => $this->t('User data'),
      'roles' => $this->t('Roles'),
      'reason' => $this->t('Reason'),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $ret = parent::prepareRow($row);

    if (in_array($row->getSourceProperty('uid'), [1])) {
      return FALSE;
    }

    $roles = $row->getSourceProperty('roles');
    $d8Roles = [];

    foreach ($roles as $rid) {
      $role = \Drupal::database()->query("select destid1 from migrate_map_upgrade_d7_user_role where sourceid1 = {$rid};")->fetchField();
      if ($rid == 1) {
        $role = 'anonymous';
      }
      elseif ($rid == 2) {
        $role = 'authenticated';
      }
      elseif ($rid == 3) {
        $role = 'administrator';
      }
      if (empty($role)) {
        continue;
      }
      $d8Roles[] = $role;
    }

    $row->setSourceProperty('roles', $d8Roles);

    return $ret;
  }

}
