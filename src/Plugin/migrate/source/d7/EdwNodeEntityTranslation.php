<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\NodeEntityTranslation;

/**
 * Provides Drupal 7 node entity translations source plugin.
 *
 * @MigrateSource(
 *   id = "edw_d7_node_entity_translation",
 *   source_module = "entity_translation"
 * )
 */
class EdwNodeEntityTranslation extends NodeEntityTranslation {

  use EdwSource;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('entity_translation', 'et')
      ->fields('et', [
        'entity_type',
        'entity_id',
        'language',
        'source',
        'uid',
        'status',
        'translate',
        'created',
        'changed',
      ])
      ->fields('n', [
        'title',
        'type',
        'promote',
        'sticky',
        'vid',
      ])
      ->fields('nr', [
        'log',
        'timestamp',
      ])
      ->condition('et.entity_type', 'node')
      ->condition('et.source', '', '<>')
      ->condition('et.language', 'und', '<>');

    $query->addField('nr', 'uid', 'revision_uid');

    $query->innerJoin('node', 'n', 'n.nid = et.entity_id');
    $query->innerJoin('node_revision', 'nr', 'nr.vid = n.vid');

    if (isset($this->configuration['node_type'])) {
      $query->condition('n.type', $this->configuration['node_type']);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $ret = parent::prepareRow($row);

    if ($this->isSkippableRow($row)) {
      return FALSE;
    }
    $this->setUrlAlias($row);

    return $ret;
  }

}
