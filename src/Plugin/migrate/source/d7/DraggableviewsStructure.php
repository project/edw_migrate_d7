<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\source\d7;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 node source from database.
 *
 * @MigrateSource(
 *   id = "draggableviews",
 *   source_module = "draggableviews"
 * )
 */
class DraggableviewsStructure extends DrupalSqlBase {

  /**
   *
   */
  public function query() {
    $query = $this->select('draggableviews_structure', 'd')
      ->fields('d')
      ->orderBy('d.dvid');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'dvid' => [
        'type' => 'integer',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return [
      'dvid' => $this->t('The primarty identifier'),
      'view_name' => $this->t('The view name.'),
      'view_display' => $this->t('The view display.'),
      'args' => $this->t('The arguments.'),
      'entity_id' => $this->t('The entity id.'),
      'weight' => $this->t('The order weight.'),
      'parent' => $this->t('The parent entity id.'),
    ];
  }

}
