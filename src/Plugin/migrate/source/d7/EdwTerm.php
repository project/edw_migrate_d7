<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\taxonomy\Plugin\migrate\source\d7\Term;

/**
 * Taxonomy term source from database.
 *
 * @MigrateSource(
 *   id = "edw_d7_taxonomy_term",
 *   source_module = "taxonomy"
 * )
 */
class EdwTerm extends Term {

  use EdwSource;

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $ret = parent::prepareRow($row);

    $tid = $row->getSourceProperty('tid');
    $vocabulary = $row->getSourceProperty('machine_name');
    $default_language = (array) $this->variableGet('language_default', ['language' => 'en']);

    // If this entity was translated using Entity Translation, we need to get
    // its source language to get the field values in the right language.
    // The translations will be migrated by the d7_node_entity_translation
    // migration.
    $entity_translatable = !empty($this->configuration['entity_translation_taxonomy']);

    if ($entity_translatable) {
      $source_language = $this->getEntityTranslationSourceLanguage('taxonomy_term', $tid);
      $language = $entity_translatable && $source_language ? $source_language : $default_language['language'];
    }
    // If this is an i18n translation use the default language when i18n_mode
    // is localized.
    if ($row->get('i18n_mode')) {
      $language = ($row->get('i18n_mode') === '1') ? $default_language['language'] : $row->get('language');
    }

    $language = $language ?? $default_language['language'];
    $row->setSourceProperty('language', $language);

    // Get Field API field values.
    foreach ($this->getFields('taxonomy_term', $vocabulary) as $field_name => $field) {
      // Ensure we're using the right language if the entity and the field are
      // translatable.
      $field_language = $field['translatable'] ? $language : NULL;
      $row->setSourceProperty($field_name, $this->getFieldValues('taxonomy_term', $field_name, $tid, NULL, $field_language));
    }

    // Find parents for this row.
    $parents = $this->select('taxonomy_term_hierarchy', 'th')
      ->fields('th', ['parent', 'tid'])
      ->condition('tid', $row->getSourceProperty('tid'))
      ->execute()
      ->fetchCol();
    $parent = reset($parents);
    $row->setSourceProperty('parent', $parent);

    // Determine if this is a forum container.
    $forum_container_tids = $this->variableGet('forum_containers', []);
    $current_tid = $row->getSourceProperty('tid');
    $row->setSourceProperty('is_container', in_array($current_tid, $forum_container_tids));

    // If the term name or term description were replaced by real fields using
    // the Drupal 7 Title module, use the fields value instead of the term name
    // or term description.
    if ($this->moduleExists('title')) {
      $name_field = $row->getSourceProperty('name_field');
      if (isset($name_field[0]['value'])) {
        $row->setSourceProperty('name', $name_field[0]['value']);
      }
      $description_field = $row->getSourceProperty('description_field');
      if (isset($description_field[0]['value'])) {
        $row->setSourceProperty('description', $description_field[0]['value']);
      }
      if (isset($description_field[0]['format'])) {
        $row->setSourceProperty('format', $description_field[0]['format']);
      }
    }

    $row->setSourceProperty('description_field', [
      [
        'value' => $row->getSourceProperty('description'),
      ]
    ]);

    $this->setUrlAlias($row);

    return $ret;
  }

}
