<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\node\Plugin\migrate\source\d7\Node;

/**
 * Drupal 7 node source from database.
 *
 * @MigrateSource(
 *   id = "edw_d7_node",
 *   source_module = "node"
 * )
 */
class EdwNode extends Node {

  use EdwSource;

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $ret = parent::prepareRow($row);

    if ($this->isSkippableRow($row)) {
      return FALSE;
    }
    $this->setUrlAlias($row);

    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFieldValues($entity_type, $field, $entity_id, $revision_id = NULL, $language = NULL) {
    // For some obscure reason, the field_revision_FIELD_XXX table in D7 does not always
    // contain the field values. As such, we need to set revision_id as NULL
    // for this function, so that it will search in field_data_FIELD_XXX instead.
    return parent::getFieldValues($entity_type, $field, $entity_id, NULL, $language);
  }

  /**
   * Retrieve field collection fields.
   */
  protected function setFieldCollectionFields(Row $row, array $nodeFcFields, array $fcFields) {
    foreach ($nodeFcFields as $instFcField) {
      $fc = $row->getSourceProperty($instFcField);
      if (empty($fc)) {
        continue;
      }
      $fcFieldValues = [];
      foreach ($fc as $idx => $fcItem) {
        $fcEntityId = $fcItem['value'];
        $fcRevisionId = $fcItem['revision_id'];
        foreach ($fcFields as $field) {
          $values = $this->getFieldValues('field_collection_item', $field, $fcEntityId, $fcRevisionId);
          $fcFieldValues[$idx][$field] = $values;
        }
      }
      $row->setSourceProperty($instFcField, $fcFieldValues);
    }
  }

  /**
   * Get a node title by nid.
   */
  protected function getNodeName($nid) {
    $query = $this->select('node', 'n')
      ->fields('n', ['title'])
      ->condition('n.nid', $nid);
    return $query->execute()->fetchField();
  }

  /**
   * Get a term name by tid.
   */
  protected function getTermName($tid) {
    $query = $this->select('taxonomy_term_data', 't')
      ->fields('t', ['name'])
      ->condition('t.tid', $tid);
    return $query->execute()->fetchField();
  }

}
