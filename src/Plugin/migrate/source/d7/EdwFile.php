<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\source\d7;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\file\Plugin\migrate\source\d7\File;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Drupal 7 file source from database.
 *
 * @MigrateSource(
 *   id = "edw_d7_file",
 *   source_module = "file"
 * )
 */
class EdwFile extends File {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $ret = parent::initializeIterator();

    $this->publicPath = $this->configuration['constants']['file_public_path'] ?? 'sites/default/files';
    $this->privatePath = $this->configuration['constants']['file_private_path'] ?? NULL;
    $this->temporaryPath = $this->configuration['constants']['file_temporary_path'] ?? 'tmp';

    return $ret;
  }

}
