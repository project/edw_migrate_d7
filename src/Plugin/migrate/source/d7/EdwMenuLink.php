<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\source\d7;

use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\menu_link_content\Plugin\migrate\source\MenuLink;
use Drupal\migrate\Row;

/**
 * Drupal menu link source from database.
 *
 * @MigrateSource(
 *   id = "edw_d7_menu_link",
 *   source_module = "menu"
 * )
 */
class EdwMenuLink extends MenuLink {

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $ret = parent::prepareRow($row);

    $id = $row->getSourceProperty('mlid');
    if (!empty($blockContents[$id])) {
      $options = $row->getSourceProperty('options');
      $options['custom_settings']['block_content'] = $blockContents[$id];
      $row->setSourceProperty('options', $options);
    }

    if (!empty($replaceLinks[$id])) {
      $oldMenuLinkContent = MenuLinkContent::load($replaceLinks[$id]);
      if (!empty($oldMenuLinkContent)) {
        $oldMenuLinkContent->delete();
      }
    }

    $link = $row->getSourceProperty('link_path');
    if ($link == '<void>' || $link == '<void1>') {
      $link = '<nolink>';
    }
    $row->setSourceProperty('link_path', $link);

    return $ret;
  }

}
