<?php

namespace Drupal\edw_migrate_d7\Plugin\migrate\destination\d7;

use Drupal\Core\Database\Database;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;

/**
 * Defines destination plugin for Content access.
 *
 * @MigrateDestination(
 *   id = "content_access"
 * )
 */
class ContentAccess extends DestinationBase {

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $record = [
      'nid' => $row->getDestinationProperty('nid'),
      'settings' => $row->getDestinationProperty('settings'),
    ];
    Database::getConnection()->delete('content_access')->condition('nid', $record['nid'])->execute();
    $result = Database::getConnection()->insert('content_access')->fields($record)->execute();
    return [$result];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'nid' => [
        'type' => 'integer',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return [
      'nid' => $this->t('The node ID'),
      'settings' => $this->t('The content access settings.'),
    ];
  }

}
